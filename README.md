# Proyecto Web Red Bicicletas del Curso de Desarrollo Fullstack con NodeJS

## Resumen
Este respositorio contiene el proyecto de red bicicletas del curso de desarrollo fullstack de la Universidad Austrual

## Instrucciones
Descargue el respositorio y ejecute el comando npm install para descargar las dependencias utilizadas

## Licencia
Este proyecto esta bajo licencia MIT, la cual esta contenida en el archivo LICENSE en el directorio raíz del proyecto

## Información de contacto
Para ponerse en contacto conmigo puede escribirme al siguiente correo electronico [manuelparra@live.com.ar](mailito:manuelparra@live.com.ar)
