var express = require('express');
var router = express.Router();
var usuarioController = require('../controllers/usuarioController');

router.get('/', usuarioController.usuario_list);

router.get('/create', usuarioController.usuario_create_get);
router.post('/create', usuarioController.usuario_create_post);

router.get('/update/:id', usuarioController.usuario_update_get);
router.post('/update/:id', usuarioController.usuario_update_post);

router.post('/delete/:id', usuarioController.usuario_delete_post);

module.exports = router;
