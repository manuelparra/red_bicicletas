const express = require('express');
const router = express.Router();
const passport = require('../../config/passport');

const authController = require('../../controllers/api/authApiController');

router.post('/authenticate', authController.authenticate);
router.post('/forgot-password', authController.forgot_password);
router.post('/facebook-token', passport.authenticate('facebook-token'), authController.authFacebookToken);

module.exports = router;
