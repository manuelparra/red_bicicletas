require('dotenv').config();
var Bicicleta = require('../../models/bicicletaModel');
var Usuario = require('../../models/usuarioModel');
var request = require('request');
var mongoose = require('mongoose');
var server = require('../../bin/www');

var base_url = "http://localhost:3000";

describe('Testing API Bicicletas.', function() {
  beforeAll(function(done) {
    if (mongoose.connection.readyState > 0) {
      if (mongoose.connection.name == 'red_bicicletas') {
        console.log("Mongodb esta conectado a red_bicicletas!!, Desconectando");
        mongoose.disconnect(done); // Antes de realizar las pruebas, cierro la conexión aque se creo en app.js para utilizar la db test
      } else {
        done();
      }
    } else {
      done();
    }
  });

  beforeEach(function(done) { // Antes de ejecutar cada test
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

    mongoose.Promise = global.Promise;

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB Connection error: '));
    db.once('open', function() {
      console.log('Estamos conectados a la base de datos testdb');
      done();
    });
  });
  
  describe('GET BICICLETAS /', () => { 
    it("Status 200", (done) => {

      var usuario = Usuario.createInstance('Manuel', 'manuelparra@live.com.ar', 'carlos', 'carlos');
      Usuario.addUser(usuario, function(err, newUser) {
        if (err) console.log(err);
        
        var headers = {'content-type': 'application/json'};
        var payload = JSON.stringify({"email": "manuelparra@live.com.ar", "password": "carlos"});

        request.post({ 
          headers: headers,
          url: base_url + '/api/auth/authenticate', 
          body: payload
        }, function(error, response, body) {       
          var result = JSON.parse(body);
          var token = result.data.token
          headers = {'content-type': 'application/json', 'x-access-token': token};  
        
          request.get({
            headers: headers, 
            url: base_url + '/api/bicicletas'
          }, function(error, response, body) {
            var result = JSON.parse(body);
            expect(response.statusCode).toBe(200);
            expect(result.bicicletas.length).toBe(0);
            done();
          });
        });
      });
    });
  });

  
  describe('GET BICICLETA /read/:code', () => {
    it("Status 200", (done) => {
      
      var usuario = Usuario.createInstance('Manuel', 'manuelparra@live.com.ar', 'carlos', 'carlos');
      Usuario.addUser(usuario, function(err, newUser) {
        if (err) console.log(err);
        
        var headers = {'content-type': 'application/json'};
        var payload = JSON.stringify({"email": "manuelparra@live.com.ar", "password": "carlos"});

        request.post({ 
          headers: headers,
          url: base_url + '/api/auth/authenticate', 
          body: payload
        }, function(error, response, body) {       
          var result = JSON.parse(body);
          var token = result.data.token
          headers = {'content-type': 'application/json', 'x-access-token': token};

          var newBici = '{"code": 1, "color": "Verde", "modelo": "Urbana", "lat": 37.886610834914016, "lng": -4.791306293646539}';

          request.post({
            headers: headers, 
            url: base_url + '/api/bicicletas/create', 
            body: newBici
          }, function(error, response, body) {
            newBici = '{"code": 2, "color": "Blanco", "modelo": "Montaña", "lat": 37.885186766470405, "lng": -4.7914177352839795}';
            
            request.post({
              headers: headers, 
              url: base_url + '/api/bicicletas/create', 
              body: newBici
            }, function(error, response, body) {
              request.get({
                headers: headers, 
                url: base_url + '/api/bicicletas/read/1'
              }, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicleta.code).toBe(1);
                expect(result.bicicleta.color).toBe('Verde');
                expect(result.bicicleta.modelo).toBe('Urbana');
                expect(result.bicicleta.ubicacion[0]).toBe(37.886610834914016);
                expect(result.bicicleta.ubicacion[1]).toBe(-4.791306293646539);
                done();
              });
            });
          });
        });
      });
    });
  });
  
  describe('POST BICICLETAS /create', () => {
    it("Status 200", (done) => {
      var usuario = Usuario.createInstance('Manuel', 'manuelparra@live.com.ar', 'carlos', 'carlos');
      Usuario.addUser(usuario, function(err, newUser) {
        if (err) console.log(err);
        
        var headers = {'content-type': 'application/json'};
        var payload = JSON.stringify({"email": "manuelparra@live.com.ar", "password": "carlos"});

        request.post({ 
          headers: headers,
          url: base_url + '/api/auth/authenticate', 
          body: payload
        }, function(error, response, body) {       
          var result = JSON.parse(body);
          var token = result.data.token
          headers = {'content-type': 'application/json', 'x-access-token': token};
          
          var newBici = '{"code": 1, "color": "Verde", "modelo": "Urbana", "lat": 37.886610834914016, "lng": -4.791306293646539}';
          
          request.post({
            headers: headers, 
            url: base_url + '/api/bicicletas/create', 
            body: newBici
          }, function(error, response, body) {
            var result = JSON.parse(body);
            expect(response.statusCode).toBe(200);
            expect(result.bicicleta.color).toBe('Verde');
            done();
          });
        });
      });
    });
  });

  describe('POST BICICLETAS /update', () => {
    it("Status 200", (done) => {
      var usuario = Usuario.createInstance('Manuel', 'manuelparra@live.com.ar', 'carlos', 'carlos');
      Usuario.addUser(usuario, function(err, newUser) {
        if (err) console.log(err);
        
        var headers = {'content-type': 'application/json'};
        var payload = JSON.stringify({"email": "manuelparra@live.com.ar", "password": "carlos"});

        request.post({ 
          headers: headers,
          url: base_url + '/api/auth/authenticate', 
          body: payload
        }, function(error, response, body) {       
          var result = JSON.parse(body);
          var token = result.data.token
          headers = {'content-type': 'application/json', 'x-access-token': token};

          var newBici = '{"code": 1, "color": "Verde", "modelo": "Urbana", "lat": 37.886610834914016, "lng": -4.791306293646539}';

          request.post({
            headers: headers, 
            url: base_url + '/api/bicicletas/create', 
            body: newBici
          }, function(error, response, body) {
            
            newBici = '{"code": 2, "color": "Blanco", "modelo": "Montaña", "lat": 37.885186766470405, "lng": -4.7914177352839795}';
            
            request.post({
              headers: headers, 
              url: base_url + '/api/bicicletas/create', 
              body: newBici
            }, function(error, response, body) {
              
              var updateBici = '{"code": 2, "color": "Gris", "modelo": "Urbana", "lat": 37.885186766470405, "lng": -4.7914177352839795}';

              request.post({
                headers: headers, 
                url: base_url + '/api/bicicletas/update', 
                body: updateBici
              }, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicleta.color).toBe('Gris');
                done();
              });
            });
          });
        });
      });
    });
  });

  describe('DELETE BICICLETAS /delete/:code', () => {
    it('Status 204', (done) => {
      var usuario = Usuario.createInstance('Manuel', 'manuelparra@live.com.ar', 'carlos', 'carlos');
      Usuario.addUser(usuario, function(err, newUser) {
        if (err) console.log(err);
        
        var headers = {'content-type': 'application/json'};
        var payload = JSON.stringify({"email": "manuelparra@live.com.ar", "password": "carlos"});

        request.post({ 
          headers: headers,
          url: base_url + '/api/auth/authenticate', 
          body: payload
        }, function(error, response, body) {       
          var result = JSON.parse(body);
          var token = result.data.token
          headers = {'content-type': 'application/json', 'x-access-token': token};

          var newBici = '{"code": 1, "color": "Verde", "modelo": "Urbana", "lat": 37.886610834914016, "lng": -4.791306293646539}';
          request.post({
            headers: headers, 
            url: base_url + '/api/bicicletas/create', 
            body: newBici
          }, function(error, response, body) {
            var result = JSON.parse(body);
            request.delete({
              headers: headers, 
              url: base_url + '/api/bicicletas/delete/' + result.bicicleta.code, 
              body: newBici
            }, function(error, response, body) {
              expect(response.statusCode).toBe(204);
              done();
            });          
          });
        });
      });
    });
  });

  afterEach(function(done) { // Despues de ejecutar los tests
    Bicicleta.deleteMany({}, function(err, success) {
      if (err) console.log(err);
      Usuario.deleteMany({}, function(err, success) {
        if (err) console.log(err);
        console.log("Mongodb esta conectado testdb!!, Desconectando");
        mongoose.disconnect(done); // Despues de realizar las pruebas, cierro la conexión a la db test
      });
    });
  });
});
