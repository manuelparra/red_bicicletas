require('dotenv').config();
var Mailer = require('../../mailer/mailer');
const nodemailer = require('nodemailer');

describe('Testing Mailer', function() { 
  describe('Sending mail', () => {
    it('Send..', (done) => {
      const mailOptions = {
        from: 'no-reply@redbiciceltas.com',
        to: 'manuelparra@live.com.ar',
        subject: 'Verficación de cuenta - Red Bicicletas',
        text: 'Hello World!!',
        html: '<b>Hello World!!</b>'
      };

      Mailer.sendMail(mailOptions, function(err, info) {
        if (err) { 
          return console.log(err.message);
          done();
        }
        console.log('Un correo de verificación fue enviado a ' + mailOptions.to + '.');
        expect(true).toBe(true);
        done();
      });
    });
  });
});
