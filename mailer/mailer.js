const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;

if (process.env.NODE_ENV === 'production') {
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET
    }
  };
  console.log(options);
  mailConfig = sgTransport(options);
} else if (process.env.NODE_ENV === 'staging') {
  console.log('XXXXXXXXXXXXXXXXXXXXX');
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET
    }
  };
  mailConfig = sgTransport(options);
} else if (process.env.NODE_ENV === 'development') {
  // All emails are catches by etherreal.email
  mailConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false, 
    auth: {
      user: process.env.ETHEREAL_USER,
      pass: process.env.ETHEREAL_PASSWORD
    }
  };
}

module.exports = nodemailer.createTransport(mailConfig);
