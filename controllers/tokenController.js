var Usuario = require('../models/usuarioModel');
var Token = require('../models/tokenModel');

module.exports = {
  token_confirmation_get: function(req, res, next) {
    Token.findToken(req.params.token, function(err, token) {
      if (!token) {
        return res.status(400).send({
          type: 'not-verified', 
          msg: 'No encontramos un usuario con este token. Quizá haya expirado y deba solictar uno nuevo'
        });
      }
      Usuario.findUser(token._userId, function(err, usuario) {
        if (usuario.verificado) return res.redirect('/usuarios');
        usuario.verificado = true;
        usuario.save(function(err) {
          if (err) return res.status(500).send({msg: err.message});
          res.redirect('/')
        });
      });
    });
  }
};
