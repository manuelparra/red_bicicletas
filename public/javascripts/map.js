var mymap = L.map('main-map').setView([37.885979374841384, -4.789891329929702], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFudWVscGFycmEiLCJhIjoiY2szN3JxbTBjMDA1ZTNocXM3cjc1dTVjbiJ9.cCInwXeyIvPQKXAALtmZkg'
}).addTo(mymap);

var headers = {'content-type': 'application/json'};
var body = JSON.stringify({ "email":"manuelparra@live.com.ar", "password":"carlos" });

$.ajax({
    type: "POST",
    headers: headers,
    url: "api/auth/authenticate",
    data: body, 
    success: function(res) {
        headers = {'Content-Type': 'application/json', 'x-access-token': res.data.token};
        $.ajax({
            headers: headers,
            url: "api/bicicletas",
            type: "GET",
            dataType: "json",
            success: function(res) {
                res.bicicletas.forEach(function(bici) {
                    var marker = L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
                });
                console.log('Ubicación de las bicicletas cargadas en el mapa con exito!...');
            }, 
            error: function(err) {
                console.log(err);
            }
        });
    },
    error: function(err) {
        console.log(err);
    }
});
